<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('home', 'HomeController@index');
Route::get('home/gioithieu', 'GioiThieuController@index');
Route::get('home/cacgiaithuong', 'CacgiaithuongController@index');
Route::get('home/tinhnangchung', 'TinhnangchungController@index');
Route::get('home/giaiphapchuyennghanh', 'GiaiphapController@index');
Route::get('home/dangkydungthu', 'DangkydungthuController@index');
Route::get('home/banggiachung', 'BanggiachungController@index');
Route::get('home/khachhang', 'KhachhangController@index');
Route::get('home/download', 'DownloadController@index');
Route::get('home/tuvanungdung', 'TuvanungdungController@index');
Route::get('home/tuvanhotro', 'TuvanhotroController@index');
Route::get('home/tuvancntt', 'TuvancnttController@index');
Route::get('home/banggiasp1', 'Banggiasp1Controller@index');
Route::get('home/banggiasp2', 'Banggiasp2Controller@index');
Route::get('home/tuyendung', 'TuyendungController@index');
Route::get('home/lienhe', 'LienheController@index');
Route::get('home/tuvanungdung/chitiet', 'TintucController@index');
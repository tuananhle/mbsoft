@extends('fontend.layouts.index')
@section('content')
	<main class="main">
                  <div class="container">
                     <div class="container">
                        <div class="row">
                           <div class="col-lg-12"></div>
                        </div>
                     </div>
                     <div class="intro-box1">
                        <div class="container">
                           <div class="row row-gen-info">
                              <div class="col-sm-12">
                                 <h3 class="title-nor" style="text-transform: uppercase;">Thông tin chung</h3>
                              </div>
                              <div class="col-sm-5">
                                 <div class="box-dt">
                                    <ul>
                                       <li>
                                          <span class="bold">Tên công ty</span>
                                          Công ty Cổ phần Phần mềm Quản lý Doanh nghiệp
                                       </li>
                                       <li>
                                          <span class="bold">Tên tiếng anh</span>
                                          MBSOFT
                                       </li>
                                       <li>
                                          <span class="bold">Tên Viết tắt</span>
                                          MBSOFT
                                       </li>
                                       <li>
                                          <span class="bold">Logo:</span>
                                          <img class="img-responsive" src="{{ url('images/logo-MBSOFT.png') }}" alt="">
                                       </li>
                                    </ul>
                                 </div>
                              </div>
                              <div class="col-sm-4">
                                 <div class="box-dt">
                                    <ul>
                                       <li>
                                          <span class="bold">Ngày thành lập</span>
                                          11-06-1997
                                       </li>
                                       <li>
                                          <span class="bold">Lĩnh vực kinh doanh:</span>
                                          Phát triển, tư vấn và triển khai ứng dụng phần mềm và giải pháp quản trị doanh nghiệp trên nền tảng CNTT.
                                       </li>
                                       <li>
                                          <span class="bold">Trụ sở và các văn phòng </span>
                                          tại Hà Nội
                                       </li>
                                       <li>
                                          <span class="bold">Số nhân viên</span>
                                          hơn 400 (tính đến 31-12-2016)
                                       </li>
                                       <li>
                                          <span class="bold">Sô Khách hàng</span>
                                          hơn 15.500 (tính đến 31-12-2016)
                                       </li>
                                       <li>
                                          <span class="bold">Website:</span>
                                          www.Mbsoft.com.vn
                                       </li>
                                       
                                    </ul>
                                 </div>
                              </div>
                              <div class="col-sm-3">
                                 <div class="intro-case">
                                    <div class="intro-case-in">
                                       <img src="{{ asset('images/intro-emp.png') }}" alt="">
                                       <p>hơn <span>400</span> nhân viên</p>
                                       <small>(tính đến hết năm 2016)</small>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="intro-box1 gray">
                        <div class="container">
                           <div class="row row-gen-prod">
                              <div class="col-sm-12">
                                 <h3 class="title-nor""title-nor" style="text-transform: uppercase;">Sản phẩm - dịch vụ</h3>
                              </div>
                              <div class="col-sm-5">
                                 <div class="box-dl">
                                    <dl>
                                       <dt>Các sản phẩm của chúng tôi</dt>
                                       <dd>
                                          <p>+ <a href="{{ url('/home/banggiasp1') }}" target="_blank"><span style="color:#008080"><strong>MBSOFT Business Online</strong></span> </a>- Giải ph&aacute;p ERP tr&ecirc;n nền tảng web</p>
                                          <p>+ <a href="/home/banggiasp2" target="_blank"><span style="color:#008080"><strong>MBSOFT Business</strong></span></a> - Giải ph&aacute;p ERP tr&ecirc;n nền windows</p>
                                         
                                       </dd>
                                     </dl>
                                 </div>
                              </div>
                              <div class="col-sm-4">
                                 <div class="box-dl">
                                    <dl>
                                       <dt>Dịch vụ</dt>
                                       <dd>
                                          <p dir="ltr">+ <a href="{{ url('/home/tuvanungdung') }}" target="_blank"><span style="color:#008080">Tư vấn ứng dụng</span></a>: tư vấn v&agrave; triển khai ứng dụng phần mềm v&agrave; giải ph&aacute;p quản trị doanh nghiệp tr&ecirc;n nền tảng CNTT.</p>
                                          <p dir="ltr">+ <a href="{{ url('/home/tuvanhotro') }}" target="_blank"><span style="color:#008080">Tư vấn, hỗ trợ sử dụng</span></a>: tư vấn, hỗ trợ trong suốt qu&aacute; tr&igrave;nh sử dụng phần mềm, đ&agrave;o tạo n&acirc;ng cao...</p>
                                          <p dir="ltr">+ <a href="{{ url('/home/tuvancntt') }}" target="_blank"><span style="color:#008080">Dịch&nbsp;vụ CNTT</span></a>: dịch vụ mạng, bảo tr&igrave; hệ thống m&aacute;y t&iacute;nh, cung cấp c&aacute;c phần mềm bản quyền, cung cấp m&aacute;y chủ...</p>
                                          <p>+ &nbsp;Tư vấn triển khai ứng dụng phần mềm cho đ&agrave;o tạo m&ocirc;n kế to&aacute;n m&aacute;y cho c&aacute;c trường, c&aacute;c trung t&acirc;m đ&agrave;o tạo.</p>
                                       </dd>
                                    </dl>
                                 </div>
                              </div>
                              <div class="col-sm-3">
                                 <div class="intro-case">
                                    <div class="intro-case-in">
                                       <img src="{{ asset('images/intro-cli.png') }}" alt="Giới thiệu về MBSOFT">
                                       <p>hơn <span>15.500</span> khách hàng</p>
                                       <small>(tính đến hết năm 2016)</small>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="intro-box2">
                        <div class="container">
                           <h3 class="intro-box2-title""title-nor" style="text-transform: uppercase;">Sứ mệnh v&agrave; gi&aacute; trị cốt l&otilde;i</h3>
                           <div class="row">
                              <div class="col-sm-7">
                                 <img src="{{ asset('images/intro-val.png') }}" alt="Sứ mệnh">
                                 <h4 class="intro-box2-tt">Sứ mệnh</h4>
                                 <p dir="ltr">+ Ph&aacute;t triển v&agrave; tư vấn ứng dụng phần mềm, gi&uacute;p c&aacute;c doanh nghiệp t&aacute;c nghiệp nhanh hơn, quản trị tốt hơn nhằm tăng năng suất lao động v&agrave; tăng hiệu quả sản xuất kinh doanh.</p>
                                 <p dir="ltr">+ Ph&aacute;t triển nghề nghiệp, ph&aacute;t huy năng lực của mỗi c&aacute; nh&acirc;n, c&ugrave;ng&nbsp;tạo dựng cuộc sống hạnh ph&uacute;c.</p>
                                 <p>+ Đ&oacute;ng g&oacute;p cho cộng đồng, x&atilde; hội, g&oacute;p phần x&acirc;y dựng đất nước.</p>
                              </div>
                              <div class="col-sm-5">
                                 <img src="{{ asset('images/intro-win.png') }}" alt="Gi&aacute; trị cốt l&otilde;i">
                                 <h4 class="intro-box2-tt">Gi&aacute; trị cốt l&otilde;i</h4>
                                 <p dir="ltr">+ Chăm chỉ, Tập trung v&agrave; Ki&ecirc;n tr&igrave;.</p>
                                 <p dir="ltr">+ Học hỏi, Đổi mới v&agrave; S&aacute;ng tạo.</p>
                                 <p>+ Đồng đội, Chung sức v&agrave; Chia sẻ.</p>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="intro-box3">
                        <div class="container">
                           <div class="row row-vision">
                              <div class="col-sm-4">
                                 <img src="{{ asset('images/vision.jpg') }}" class="img-responsive" alt="Tầm nh&igrave;n">
                                 <h3 class="vision-tt">Tầm nh&igrave;n</h3>
                                 <div class="text">MBSOFT là một công ty phát triển bền vững, là nhà cung cấp tin cậy giải pháp quản trị hoạt động SXKD trên nền tảng CNTT, có trách nhiệm với cộng đồng, góp phần xây dựng đất nước, mỗi thành viên ngày càng phát triển về nghề nghiệp và có cuộc sống hạnh phúc.</div>
                              </div>
                              <div class="col-sm-8">
                                 <h3 class="title-und">Chiến lược</h3>
                                 <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                    <div class="panel panel-default">
                                       <div class="panel-heading" role="tab" id="heading0">
                                          <h4 class="panel-title">
                                             <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse-0"
                                                aria-expanded="true" aria-controls="collapse0">
                                             Quan điểm hoạt động, kinh doanh: đối t&aacute;c l&acirc;u d&agrave;i, tin cậy
                                             </a>
                                          </h4>
                                       </div>
                                       <div id="collapse-0" class="panel-collapse collapse in" role="tabpanel"
                                          aria-labelledby="heading0">
                                          <div class="panel-body">
                                             <p>Quan điểm của MBSOFT trong ph&aacute;t triển kinh doanh, x&acirc;y dựng c&aacute;c mối quan hệ với c&aacute;c đối t&aacute;c (nh&acirc;n vi&ecirc;n, kh&aacute;ch h&agrave;ng, cộng đồng): bền vững, l&acirc;u d&agrave;i, tin cậy.</p>
                                             <p>+ Đối với nh&acirc;n vi&ecirc;n: kh&ocirc;ng chỉ l&agrave; nơi l&agrave;m c&ocirc;ng ăn lương, m&agrave; c&ograve;n l&agrave; nơi ph&aacute;t triển nghề nghiệp, x&acirc;y dựng sự nghiệp, c&ocirc;ng ty l&agrave; một phần quan trọng trong việc tạo dựng cuộc sống hạnh ph&uacute;c.</p>
                                             <p>+ Đối với kh&aacute;ch h&agrave;ng: kh&ocirc;ng chỉ l&agrave; b&aacute;n h&agrave;ng, m&agrave; c&ograve;n l&agrave; kết bạn, trở th&agrave;nh đối t&aacute;c l&acirc;u d&agrave;i, hợp t&aacute;c c&ugrave;ng ph&aacute;t triển.</p>
                                             <p>+ Đối với cộng đồng, x&atilde; hội: tập trung đ&oacute;ng g&oacute;p, hỗ trợ l&acirc;u d&agrave;i v&agrave;o những địa chỉ cụ thể.</p>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="panel panel-default">
                                       <div class="panel-heading" role="tab" id="heading1">
                                          <h4 class="panel-title">
                                             <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse-1"
                                                aria-expanded="true" aria-controls="collapse1">
                                             Chiến lược cạnh tranh: Nhanh hơn - Th&ocirc;ng minh hơn
                                             </a>
                                          </h4>
                                       </div>
                                       <div id="collapse-1" class="panel-collapse collapse " role="tabpanel"
                                          aria-labelledby="heading1">
                                          <div class="panel-body">
                                             <p><em><strong><span style="color:#008080">Sản phẩm (phần mềm)</span></strong></em></p>
                                             <p>+ Nhanh hơn: Tốc độ xử l&yacute; của phần mềm ng&agrave;y c&agrave;ng nhanh, gi&uacute;p kh&aacute;ch h&agrave;ng tăng tốc độ t&aacute;c nghiệp v&agrave; khai th&aacute;c th&ocirc;ng tin, đặc biệt trong điều kiện số lượng giao dịch, cơ sở dữ liệu ng&agrave;y c&agrave;ng lớn v&agrave; hoạt động trong m&ocirc;i trường online th&ocirc;ng qua internet.</p>
                                             <p>+ Th&ocirc;ng minh hơn: C&oacute; nhiều t&iacute;nh năng, chức năng th&ocirc;ng minh, giải quyết c&aacute;c b&agrave;i to&aacute;n nghiệp vụ v&agrave; quản trị của kh&aacute;ch h&agrave;ng, gi&uacute;p kh&aacute;ch h&agrave;ng t&aacute;c nghiệp v&agrave; khai th&aacute;c dữ liệu nhanh hơn, hiệu quả hơn.</p>
                                             <p><em><span style="color:#008080"><strong>Về dịch vụ tư vấn, hỗ trợ v&agrave; chăm s&oacute;c kh&aacute;ch h&agrave;ng</strong></span></em></p>
                                             <p>+ Nhanh hơn: Thời gian ho&agrave;n th&agrave;nh c&aacute;c dự &aacute;n tư vấn ứng dụng, ho&agrave;n th&agrave;nh c&aacute;c dịch vụ hỗ trợ kh&aacute;ch h&agrave;ng ng&agrave;y c&agrave;ng nhanh hơn.</p>
                                             <p>+ Th&ocirc;ng minh hơn: Đưa ra c&aacute;c giải ph&aacute;p tư vấn ứng dụng th&ocirc;ng minh, giải quyết c&aacute;c b&agrave;i to&aacute;n nghiệp vụ v&agrave; quản trị của kh&aacute;ch h&agrave;ng, gi&uacute;p kh&aacute;ch h&agrave;ng t&aacute;c nghiệp v&agrave; khai th&aacute;c dữ liệu nhanh hơn, hiệu quả hơn.</p>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="panel panel-default">
                                       <div class="panel-heading" role="tab" id="heading2">
                                          <h4 class="panel-title">
                                             <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse-2"
                                                aria-expanded="true" aria-controls="collapse2">
                                             Phương ph&aacute;p quản trị hoạt động: Quản trị theo mục ti&ecirc;u
                                             </a>
                                          </h4>
                                       </div>
                                       <div id="collapse-2" class="panel-collapse collapse " role="tabpanel"
                                          aria-labelledby="heading2">
                                          <div class="panel-body">
                                             <p>Quản trị hoạt động kinh doanh v&agrave; c&aacute;c hoạt động của c&ocirc;ng ty theo phương ph&aacute;p quản trị theo mục ti&ecirc;u (MBO &ndash; Management by Objectives) th&ocirc;ng qua hệ thống c&aacute;c chỉ ti&ecirc;u đ&aacute;nh gi&aacute; (KPI &ndash; Key Peformance Indicators).</p>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </main>
@endsection
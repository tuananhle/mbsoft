@extends('fontend.layouts.index')
@section('content')
	<main class="main">
                  <div class="container">
                     <div class="container">
                        <div class="main-left">
                           <nav class="menu-left aside-left">
                              <h3 class="title-left">Bảng gi&aacute; sản phẩm</h3>
                              <ul>
                                 <li class="">
                                    <a href="{{ url('home/banggiasp1')}}" title="Bảng gi&aacute; Fast Accounting Online">
                                    Fast Accounting Online
                                    </a>
                                 </li>
                                 <li class="active">
                                    <a href="{{ url('home/banggiasp2')}}" title="Bảng gi&aacute; phần mềm kế to&aacute;n Fast Accounting">
                                    Fast Accounting
                                    </a>
                                 </li>
                              </ul>
                           </nav>
                        </div>
                        <div class="main-right">
                           <div class="content-detail">
                              <h1 class="title-detail">Bảng gi&aacute; Fast Accounting Online</h1>
                              <div class="detail-top">
                                 ﻿Ng&agrave;y đăng: <span class="color">2016-11-01 10:14:04</span>
                                 - Ng&agrave;y cập nhật: <span class="color">2016-11-01 10:14:04</span>
                                 - Số lần xem: <span class="color">8279</span>
                              </div>
                              <div class="detail-rating">
                                 <div class="rating-wrap ml0">
                                    <script>
                                       $(document).ready(function () {
                                           var text_rate = $('.rating-text-txt').html();
                                           var rated = false;
                                           $('input[name="rating"]').on('click', function (e) {
                                               e.preventDefault();
                                               var form_data = $(this).val();
                                       
                                               if (!rated) {
                                                   $('.rating-text-txt')
                                                       .hide()
                                                       .addClass('rating-text-loading')
                                                       .html('Đang cập nhật...')
                                                       .fadeIn();
                                       
                                                   $.ajax({
                                                       url: '/global-actions/rating/post/1097',
                                                       type: 'POST',
                                                       data: {
                                                           value: form_data
                                                       },
                                                       cache: false,
                                                       complete: function (data) {
                                                           var _data = data.responseJSON;
                                                           var total = _data.data.total,
                                                               ratings_average = parseFloat(_data.data.ratings_average).toFixed(1);
                                                           if (!_data.error) {
                                                               rated = true;
                                                               $('.rating-text-txt')
                                                                   .hide()
                                                                   .removeClass('rating-text-loading')
                                                                   .text('Cám ơn bạn đã cho điểm!')
                                                                   .fadeIn();
                                                               setTimeout(function () {
                                                                   text_rate = 'Điểm: <span class="color">' + ratings_average + '/5</span> (' + total + ' phiếu)';
                                                                   $('.rating-text-txt')
                                                                       .hide()
                                                                       .html(text_rate)
                                                                       .fadeIn();
                                                               }, 2000);
                                                           }
                                                       }
                                                   });
                                               } else {
                                                   setTimeout(function () {
                                                       $('.rating-text-txt')
                                                           .hide()
                                                           .removeClass('rating-text-loading')
                                                           .html('Bạn đã cho điểm bài này rồi!')
                                                           .fadeIn();
                                                   }, 1000);
                                                   setTimeout(function () {
                                                       $('.rating-text-txt')
                                                           .hide()
                                                           .html(text_rate)
                                                           .fadeIn();
                                                   }, 3000);
                                               }
                                           });
                                       });
                                    </script>
                                    <div class="rating-text">
                                       <span class="rating-text-txt">Điểm: <span class="color">3/5</span> (1 phiếu)</span>
                                    </div>
                                    <span class="rating">
                                    <input id="rating5" type="radio" name="rating" value="5" >
                                    <label for="rating5">5</label>
                                    <input id="rating4" type="radio" name="rating" value="4" >
                                    <label for="rating4">4</label>
                                    <input id="rating3" type="radio" name="rating" value="3" checked>
                                    <label for="rating3">3</label>
                                    <input id="rating2" type="radio" name="rating" value="2" >
                                    <label for="rating2">2</label>
                                    <input id="rating1" type="radio" name="rating" value="1" >
                                    <label for="rating1">1</label>
                                    </span>
                                 </div>
                              </div>
                              <div class="social-bxcount">
                                 <ul class="social">
                                    <li>
                                       Chia sẻ
                                    </li>
                                    <li>
                                       <div class="facebook" style="display: inline-block;">
                                          <div class="fb-like"
                                             data-href="http://fast.com.vn/bang-gia-fast-accounting-online"
                                             data-layout="button_count" data-action="like" data-size="small" data-show-faces="false" data-share="false"></div>
                                          <div class="fb-share-button" data-href="http://fast.com.vn/bang-gia-fast-accounting-online" data-layout="button_count" data-size="small" data-mobile-iframe="true">
                                             <a class="fb-xfbml-parse-ignore" target="_blank" href="http://fast.com.vn/bang-gia-fast-accounting-online&src=sdkpreparse">Share</a>
                                          </div>
                                          <div id="fb-root"></div>
                                          <script>(function(d, s, id) {
                                             var js, fjs = d.getElementsByTagName(s)[0];
                                             if (d.getElementById(id)) return;
                                             js = d.createElement(s); js.id = id;
                                             js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.7";
                                             fjs.parentNode.insertBefore(js, fjs);
                                             }(document, 'script', 'facebook-jssdk'));
                                          </script>
                                       </div>
                                    </li>
                                    <li>
                                       <!-- Place this tag in your head or just before your close body tag. -->
                                       <script src="https://apis.google.com/js/platform.js" async defer></script>
                                       <!-- Place this tag where you want the share button to render. -->
                                       <div class="g-plus" data-action="share" data-annotation="bubble" data-height="24"></div>
                                    </li>
                                 </ul>
                              </div>
                              <article class="article-content">
                                 <style scoped>
                                    a {
                                    color : #00b6bc;
                                    }
                                 </style>
                                
                                 <div class="content-after">
                                 </div>
                                 
                              </article>
                           </div>
                           
                           <div class="article-comment">
                              <style>
                                 #comments .media-list.show-all-parent .media.hidden:not(.media2) {
                                 display : block !important;
                                 }
                                 #comments .media-list .group.show-all .media.hidden {
                                 display : block !important;
                                 }
                              </style>
                              <div class="facebook-comment">
                                 <h4 class="article-tt-other">Gửi b&igrave;nh luận</h4>
                                 <form method="POST" id="form-comment" accept-charset="UTF-8" action="/global-actions/create-comment/post/1097">
                                    <input type="hidden" name="_token" value="os15hPKGJ4spR5wDy4Zfu4k8uZrazwfVznPcJe7M">
                                    <div class="row">
                                       <div class="col-sm-4">
                                          <div class="form-group">
                                             <input type="text" name="name" class="form-control" value=""
                                                placeholder="Họ v&agrave; t&ecirc;n *" required autocomplete="off">
                                          </div>
                                       </div>
                                       <div class="col-sm-4">
                                          <div class="form-group">
                                             <input type="text" name="email" class="form-control" value=""
                                                placeholder="Email *" required autocomplete="off">
                                          </div>
                                       </div>
                                       <div class="col-sm-4">
                                          <div class="form-group">
                                             <input type="text" name="phone" class="form-control" value=""
                                                placeholder="Số điện thoại *" required autocomplete="off">
                                          </div>
                                       </div>
                                    </div>
                                    <div class="row">
                                       <div class="col-md-12">
                                          <div class="form-group">
                                             <textarea class="form-control" rows="5" name="content" placeholder="Nội dung"></textarea>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="row">
                                       <div class="col-md-12 text-right">
                                          <button type="submit" class="btn btn-primary">Gửi b&igrave;nh luận</button>
                                       </div>
                                    </div>
                                 </form>
                                 <h4 class="article-tt-other">6 B&igrave;nh luận</h4>
                                 <div class="row">
                                    <div class="col-md-12">
                                       <div id="comments">
                                          <div class="media-list">
                                             <script>
                                                function voteComment(id, type) {
                                                    var el = $('[data-vote-comment=' + id + ']');
                                                    var num = el.find('.vote-comment-txt').attr('vote-comment-count');
                                                    num = +num + 1;
                                                    if (type === 'down') {
                                                        $data = {type: 'dislike'};
                                                
                                                        el.find('.hasTip-down').text('(' + num + ')');
                                                        el.find('.votedown-btn').addClass('active');
                                                    } else {
                                                        $data = {type: 'like'};
                                                        el.find('.hasTip-up').text('(' + num + ')');
                                                        el.find('.voteup-btn').addClass('active');
                                                    }
                                                    $.ajax({
                                                        url: '/global-actions/rating-comment/' + id,
                                                        type: 'POST',
                                                        data: $data,
                                                        cache: false,
                                                        complete: function (data) {
                                                            var _data = data.responseJSON;
                                                            if (!_data.error) {
                                                                var like_count = _data.data.like_count,
                                                                        dislike_count = _data.data.dislike_count;
                                                
                                                                el.find('.hasTip-up').text('(' + like_count + ')');
                                                                el.find('.hasTip-down').text('(' + dislike_count + ')');
                                                
                                                                if (type === 'down') {
                                                                    el.find('.votedown-btn').addClass('active');
                                                                } else {
                                                                    el.find('.voteup-btn').addClass('active');
                                                                }
                                                
                                                                el.find('.hasTip').removeAttr('href');
                                                            }
                                                        }
                                                    });
                                                }
                                                $(document).ready(function () {
                                                    $('body').on('click', '.fbs-vote-comment > .media-heading-toggle', function (event) {
                                                        var $current = $(this);
                                                        var $target = $($current.attr('href'));
                                                
                                                        $target.find('textarea').html('@' + $current.attr('data-name') + ': ');
                                                    });
                                                });
                                             </script>
                                             <div class="media  ">
                                                <div class="media-left">
                                                   <a href="#">
                                                   <img src=images/ad1.png" alt="" class="media-object">
                                                   </a>
                                                </div>
                                                <div class="media-body">
                                                   <h4 class="media-heading">
                                                      Trương Vũ Kh&aacute;nh Ly
                                                      <span>2018-03-05 14:41:03</span>
                                                      <span data-vote-comment="1646" class="fbs-vote-comment">
                                                      <a role="button" data-toggle="collapse" href="#comments-1646" aria-expanded="false"
                                                         aria-controls="comments-1646" class="media-heading-toggle"
                                                         data-name="Trương Vũ Kh&aacute;nh Ly">open</a>
                                                      <a href="javascript:voteComment(1646, 'up')" title="Th&iacute;ch"
                                                         class="voteup-btn hasTip">
                                                      <i vote-comment-count="0"
                                                         class="vote-comment-txt hasTip-up">(0)</i>
                                                      </a>
                                                      <a href="javascript:voteComment(1646, 'down')" title="Kh&ocirc;ng th&iacute;ch"
                                                         class="votedown-btn hasTip">
                                                      <i vote-comment-count="0"
                                                         class="vote-comment-txt hasTip-down">(0)</i>
                                                      </a>
                                                      </span>
                                                   </h4>
                                                   <div class="clearfix">Mình muốn được tư vấn về phần mềm. Vui lòng gọi qua số dt: 090334XXXX</div>
                                                </div>
                                             </div>
                                             <div class="group">
                                                <div class="media media2 ">
                                                   <div class="media-left">
                                                      <a href="#">
                                                      <img src=images/ad1.png" alt="" class="media-object">
                                                      </a>
                                                   </div>
                                                   <div class="media-body">
                                                      <h4 class="media-heading">
                                                         C&Ocirc;NG TY FAST
                                                         <span>2018-03-06 08:42:45</span>
                                                         <span data-vote-comment="1649" class="fbs-vote-comment">
                                                         <a href="javascript:voteComment(1649, 'up')" title="Th&iacute;ch"
                                                            class="voteup-btn hasTip">
                                                         <i vote-comment-count="0"
                                                            class="vote-comment-txt hasTip-up">(0)</i>
                                                         </a>
                                                         <a href="javascript:voteComment(1649, 'down')" title="Kh&ocirc;ng th&iacute;ch"
                                                            class="votedown-btn hasTip">
                                                         <i vote-comment-count="0"
                                                            class="vote-comment-txt hasTip-down">(0)</i>
                                                         </a>
                                                         </span>
                                                      </h4>
                                                      <div class="clearfix">@Trương Vũ Khánh Ly: Chào chị Ly,<br />
                                                         <br />
                                                         Cảm ơn chị đã quan tâm đến phần mềm FAST. FAST sẽ liên hệ tư vấn trực tiếp nhé.<br />
                                                         Trân trọng.
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="collapse" id="comments-1646">
                                                <form method="POST" id="form-comment" accept-charset="UTF-8"
                                                   action="/global-actions/create-comment/post/1097">
                                                   <input type="hidden" name="_token" value="os15hPKGJ4spR5wDy4Zfu4k8uZrazwfVznPcJe7M">
                                                   <input type="hidden" name="parent_id" value="1646">
                                                   <div class="media media2">
                                                      <div class="form-group">
                                                         <input type="text" name="name" class="form-control" value=""
                                                            placeholder="Họ v&agrave; t&ecirc;n *" required
                                                            autocomplete="off">
                                                      </div>
                                                      <div class="form-group">
                                                         <input type="text" name="email" class="form-control" value=""
                                                            placeholder="Email *" required autocomplete="off">
                                                      </div>
                                                      <div class="form-group">
                                                         <input type="text" name="phone" class="form-control" value=""
                                                            placeholder="Số điện thoại *" required autocomplete="off">
                                                      </div>
                                                      <div class="form-group">
                                                         <textarea class="form-control" rows="5" name="content"
                                                            placeholder="Nội dung"></textarea>
                                                      </div>
                                                      <div class="text-right">
                                                         <button type="submit"
                                                            class="btn btn-primary">Gửi b&igrave;nh luận</button>
                                                      </div>
                                                   </div>
                                                </form>
                                             </div>
                                             <div class="media  ">
                                                <div class="media-left">
                                                   <a href="#">
                                                   <img src=images/ad1.png" alt="" class="media-object">
                                                   </a>
                                                </div>
                                                <div class="media-body">
                                                   <h4 class="media-heading">
                                                      Nguyễn Văn H&ugrave;ng
                                                      <span>2017-10-30 11:02:22</span>
                                                      <span data-vote-comment="1284" class="fbs-vote-comment">
                                                      <a role="button" data-toggle="collapse" href="#comments-1284" aria-expanded="false"
                                                         aria-controls="comments-1284" class="media-heading-toggle"
                                                         data-name="Nguyễn Văn H&ugrave;ng">open</a>
                                                      <a href="javascript:voteComment(1284, 'up')" title="Th&iacute;ch"
                                                         class="voteup-btn hasTip">
                                                      <i vote-comment-count="0"
                                                         class="vote-comment-txt hasTip-up">(0)</i>
                                                      </a>
                                                      <a href="javascript:voteComment(1284, 'down')" title="Kh&ocirc;ng th&iacute;ch"
                                                         class="votedown-btn hasTip">
                                                      <i vote-comment-count="0"
                                                         class="vote-comment-txt hasTip-down">(0)</i>
                                                      </a>
                                                      </span>
                                                   </h4>
                                                   <div class="clearfix">Mình cần tư vấn phần mềm kế toán online, nhưng chat tư vấn trực tuyến không có nhân viên nào trả lời. Vậy tư vấn online có còn sử dụng được hay không.</div>
                                                </div>
                                             </div>
                                             <div class="group">
                                                <div class="media media2 ">
                                                   <div class="media-left">
                                                      <a href="#">
                                                      <img src=images/ad1.png" alt="" class="media-object">
                                                      </a>
                                                   </div>
                                                   <div class="media-body">
                                                      <h4 class="media-heading">
                                                         C&Ocirc;NG TY FAST
                                                         <span>2017-10-31 08:21:33</span>
                                                         <span data-vote-comment="1285" class="fbs-vote-comment">
                                                         <a href="javascript:voteComment(1285, 'up')" title="Th&iacute;ch"
                                                            class="voteup-btn hasTip">
                                                         <i vote-comment-count="0"
                                                            class="vote-comment-txt hasTip-up">(0)</i>
                                                         </a>
                                                         <a href="javascript:voteComment(1285, 'down')" title="Kh&ocirc;ng th&iacute;ch"
                                                            class="votedown-btn hasTip">
                                                         <i vote-comment-count="0"
                                                            class="vote-comment-txt hasTip-down">(0)</i>
                                                         </a>
                                                         </span>
                                                      </h4>
                                                      <div class="clearfix">@Nguyễn Văn Hùng: Chào anh Hùng,<br />
                                                         <br />
                                                         Rất xin lỗi anh vì vấn đề trên. FAST sẽ liên hệ để tư vấn trực tiếp anh nhé.<br />
                                                         Trân trọng,
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="collapse" id="comments-1284">
                                                <form method="POST" id="form-comment" accept-charset="UTF-8"
                                                   action="/global-actions/create-comment/post/1097">
                                                   <input type="hidden" name="_token" value="os15hPKGJ4spR5wDy4Zfu4k8uZrazwfVznPcJe7M">
                                                   <input type="hidden" name="parent_id" value="1284">
                                                   <div class="media media2">
                                                      <div class="form-group">
                                                         <input type="text" name="name" class="form-control" value=""
                                                            placeholder="Họ v&agrave; t&ecirc;n *" required
                                                            autocomplete="off">
                                                      </div>
                                                      <div class="form-group">
                                                         <input type="text" name="email" class="form-control" value=""
                                                            placeholder="Email *" required autocomplete="off">
                                                      </div>
                                                      <div class="form-group">
                                                         <input type="text" name="phone" class="form-control" value=""
                                                            placeholder="Số điện thoại *" required autocomplete="off">
                                                      </div>
                                                      <div class="form-group">
                                                         <textarea class="form-control" rows="5" name="content"
                                                            placeholder="Nội dung"></textarea>
                                                      </div>
                                                      <div class="text-right">
                                                         <button type="submit"
                                                            class="btn btn-primary">Gửi b&igrave;nh luận</button>
                                                      </div>
                                                   </div>
                                                </form>
                                             </div>
                                             <div class="media  ">
                                                <div class="media-left">
                                                   <a href="#">
                                                   <img src=images/ad1.png" alt="" class="media-object">
                                                   </a>
                                                </div>
                                                <div class="media-body">
                                                   <h4 class="media-heading">
                                                      thuỳ dương
                                                      <span>2017-08-28 15:43:54</span>
                                                      <span data-vote-comment="1074" class="fbs-vote-comment">
                                                      <a role="button" data-toggle="collapse" href="#comments-1074" aria-expanded="false"
                                                         aria-controls="comments-1074" class="media-heading-toggle"
                                                         data-name="thuỳ dương">open</a>
                                                      <a href="javascript:voteComment(1074, 'up')" title="Th&iacute;ch"
                                                         class="voteup-btn hasTip">
                                                      <i vote-comment-count="0"
                                                         class="vote-comment-txt hasTip-up">(0)</i>
                                                      </a>
                                                      <a href="javascript:voteComment(1074, 'down')" title="Kh&ocirc;ng th&iacute;ch"
                                                         class="votedown-btn hasTip">
                                                      <i vote-comment-count="0"
                                                         class="vote-comment-txt hasTip-down">(0)</i>
                                                      </a>
                                                      </span>
                                                   </h4>
                                                   <div class="clearfix">mình muốn mua phần mềm kế toán, bạn tư vấn giúp mình nhé</div>
                                                </div>
                                             </div>
                                             <div class="group">
                                                <div class="media media2 ">
                                                   <div class="media-left">
                                                      <a href="#">
                                                      <img src=images/ad1.png" alt="" class="media-object">
                                                      </a>
                                                   </div>
                                                   <div class="media-body">
                                                      <h4 class="media-heading">
                                                         C&Ocirc;NG TY FAST
                                                         <span>2017-08-29 09:02:06</span>
                                                         <span data-vote-comment="1076" class="fbs-vote-comment">
                                                         <a href="javascript:voteComment(1076, 'up')" title="Th&iacute;ch"
                                                            class="voteup-btn hasTip">
                                                         <i vote-comment-count="0"
                                                            class="vote-comment-txt hasTip-up">(0)</i>
                                                         </a>
                                                         <a href="javascript:voteComment(1076, 'down')" title="Kh&ocirc;ng th&iacute;ch"
                                                            class="votedown-btn hasTip">
                                                         <i vote-comment-count="0"
                                                            class="vote-comment-txt hasTip-down">(0)</i>
                                                         </a>
                                                         </span>
                                                      </h4>
                                                      <div class="clearfix">@thuỳ dương: Chào chị Dương,<br />
                                                         <br />
                                                         Cảm ơn bạn đã quan tâm đến phần mềm FAST. FAST sẽ liên hệ tư vấn trực tiếp nhé.<br />
                                                         Trân trọng.<br />
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="collapse" id="comments-1074">
                                                <form method="POST" id="form-comment" accept-charset="UTF-8"
                                                   action="/global-actions/create-comment/post/1097">
                                                   <input type="hidden" name="_token" value="os15hPKGJ4spR5wDy4Zfu4k8uZrazwfVznPcJe7M">
                                                   <input type="hidden" name="parent_id" value="1074">
                                                   <div class="media media2">
                                                      <div class="form-group">
                                                         <input type="text" name="name" class="form-control" value=""
                                                            placeholder="Họ v&agrave; t&ecirc;n *" required
                                                            autocomplete="off">
                                                      </div>
                                                      <div class="form-group">
                                                         <input type="text" name="email" class="form-control" value=""
                                                            placeholder="Email *" required autocomplete="off">
                                                      </div>
                                                      <div class="form-group">
                                                         <input type="text" name="phone" class="form-control" value=""
                                                            placeholder="Số điện thoại *" required autocomplete="off">
                                                      </div>
                                                      <div class="form-group">
                                                         <textarea class="form-control" rows="5" name="content"
                                                            placeholder="Nội dung"></textarea>
                                                      </div>
                                                      <div class="text-right">
                                                         <button type="submit"
                                                            class="btn btn-primary">Gửi b&igrave;nh luận</button>
                                                      </div>
                                                   </div>
                                                </form>
                                             </div>
                                             <div class="media  ">
                                                <div class="media-left">
                                                   <a href="#">
                                                   <img src=images/ad1.png" alt="" class="media-object">
                                                   </a>
                                                </div>
                                                <div class="media-body">
                                                   <h4 class="media-heading">
                                                      nguyễn Thị Minh
                                                      <span>2017-08-22 17:02:11</span>
                                                      <span data-vote-comment="1049" class="fbs-vote-comment">
                                                      <a role="button" data-toggle="collapse" href="#comments-1049" aria-expanded="false"
                                                         aria-controls="comments-1049" class="media-heading-toggle"
                                                         data-name="nguyễn Thị Minh">open</a>
                                                      <a href="javascript:voteComment(1049, 'up')" title="Th&iacute;ch"
                                                         class="voteup-btn hasTip">
                                                      <i vote-comment-count="0"
                                                         class="vote-comment-txt hasTip-up">(0)</i>
                                                      </a>
                                                      <a href="javascript:voteComment(1049, 'down')" title="Kh&ocirc;ng th&iacute;ch"
                                                         class="votedown-btn hasTip">
                                                      <i vote-comment-count="0"
                                                         class="vote-comment-txt hasTip-down">(0)</i>
                                                      </a>
                                                      </span>
                                                   </h4>
                                                   <div class="clearfix">mình muốn mua phần mềm Fast bạn tư vấn giúp mình nhé</div>
                                                </div>
                                             </div>
                                             <div class="group">
                                                <div class="media media2 ">
                                                   <div class="media-left">
                                                      <a href="#">
                                                      <img src=images/ad1.png" alt="" class="media-object">
                                                      </a>
                                                   </div>
                                                   <div class="media-body">
                                                      <h4 class="media-heading">
                                                         C&Ocirc;NG TY FAST 
                                                         <span>2017-08-23 08:36:42</span>
                                                         <span data-vote-comment="1051" class="fbs-vote-comment">
                                                         <a href="javascript:voteComment(1051, 'up')" title="Th&iacute;ch"
                                                            class="voteup-btn hasTip">
                                                         <i vote-comment-count="0"
                                                            class="vote-comment-txt hasTip-up">(0)</i>
                                                         </a>
                                                         <a href="javascript:voteComment(1051, 'down')" title="Kh&ocirc;ng th&iacute;ch"
                                                            class="votedown-btn hasTip">
                                                         <i vote-comment-count="0"
                                                            class="vote-comment-txt hasTip-down">(0)</i>
                                                         </a>
                                                         </span>
                                                      </h4>
                                                      <div class="clearfix">@Nguyễn Thị Minh: Chào chị Minh,<br />
                                                         <br />
                                                         Cảm ơn chị đã quan tâm đến phần mềm FAST. FAST sẽ liên hệ và tư vấn đến chị nhé.<br />
                                                         Trân trọng.
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="collapse" id="comments-1049">
                                                <form method="POST" id="form-comment" accept-charset="UTF-8"
                                                   action="/global-actions/create-comment/post/1097">
                                                   <input type="hidden" name="_token" value="os15hPKGJ4spR5wDy4Zfu4k8uZrazwfVznPcJe7M">
                                                   <input type="hidden" name="parent_id" value="1049">
                                                   <div class="media media2">
                                                      <div class="form-group">
                                                         <input type="text" name="name" class="form-control" value=""
                                                            placeholder="Họ v&agrave; t&ecirc;n *" required
                                                            autocomplete="off">
                                                      </div>
                                                      <div class="form-group">
                                                         <input type="text" name="email" class="form-control" value=""
                                                            placeholder="Email *" required autocomplete="off">
                                                      </div>
                                                      <div class="form-group">
                                                         <input type="text" name="phone" class="form-control" value=""
                                                            placeholder="Số điện thoại *" required autocomplete="off">
                                                      </div>
                                                      <div class="form-group">
                                                         <textarea class="form-control" rows="5" name="content"
                                                            placeholder="Nội dung"></textarea>
                                                      </div>
                                                      <div class="text-right">
                                                         <button type="submit"
                                                            class="btn btn-primary">Gửi b&igrave;nh luận</button>
                                                      </div>
                                                   </div>
                                                </form>
                                             </div>
                                             <div class="media  ">
                                                <div class="media-left">
                                                   <a href="#">
                                                   <img src=images/ad1.png" alt="" class="media-object">
                                                   </a>
                                                </div>
                                                <div class="media-body">
                                                   <h4 class="media-heading">
                                                      Tạ Thị Thu Trang
                                                      <span>2017-06-29 13:24:25</span>
                                                      <span data-vote-comment="780" class="fbs-vote-comment">
                                                      <a role="button" data-toggle="collapse" href="#comments-780" aria-expanded="false"
                                                         aria-controls="comments-780" class="media-heading-toggle"
                                                         data-name="Tạ Thị Thu Trang">open</a>
                                                      <a href="javascript:voteComment(780, 'up')" title="Th&iacute;ch"
                                                         class="voteup-btn hasTip">
                                                      <i vote-comment-count="0"
                                                         class="vote-comment-txt hasTip-up">(0)</i>
                                                      </a>
                                                      <a href="javascript:voteComment(780, 'down')" title="Kh&ocirc;ng th&iacute;ch"
                                                         class="votedown-btn hasTip">
                                                      <i vote-comment-count="0"
                                                         class="vote-comment-txt hasTip-down">(0)</i>
                                                      </a>
                                                      </span>
                                                   </h4>
                                                   <div class="clearfix">mình muốn cài PM cho DN SX ( có 1 máy chủ và 3  máy trạm) báo giá cho mình nha.<br /></div>
                                                </div>
                                             </div>
                                             <div class="group">
                                                <div class="media media2 ">
                                                   <div class="media-left">
                                                      <a href="#">
                                                      <img src=images/ad1.png" alt="" class="media-object">
                                                      </a>
                                                   </div>
                                                   <div class="media-body">
                                                      <h4 class="media-heading">
                                                         C&Ocirc;NG TY FAST
                                                         <span>2017-06-30 09:42:02</span>
                                                         <span data-vote-comment="789" class="fbs-vote-comment">
                                                         <a href="javascript:voteComment(789, 'up')" title="Th&iacute;ch"
                                                            class="voteup-btn hasTip">
                                                         <i vote-comment-count="0"
                                                            class="vote-comment-txt hasTip-up">(0)</i>
                                                         </a>
                                                         <a href="javascript:voteComment(789, 'down')" title="Kh&ocirc;ng th&iacute;ch"
                                                            class="votedown-btn hasTip">
                                                         <i vote-comment-count="0"
                                                            class="vote-comment-txt hasTip-down">(0)</i>
                                                         </a>
                                                         </span>
                                                      </h4>
                                                      <div class="clearfix">@Tạ Thị Thu Trang: Chào chị Trang,<br />
                                                         <br />
                                                         Cảm ơn chị đã quan tâm đến phần mềm FAST. FAST sẽ liên hệ tư vấn trực tiếp nhé.<br />
                                                         Trân trọng.<br />
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="collapse" id="comments-780">
                                                <form method="POST" id="form-comment" accept-charset="UTF-8"
                                                   action="/global-actions/create-comment/post/1097">
                                                   <input type="hidden" name="_token" value="os15hPKGJ4spR5wDy4Zfu4k8uZrazwfVznPcJe7M">
                                                   <input type="hidden" name="parent_id" value="780">
                                                   <div class="media media2">
                                                      <div class="form-group">
                                                         <input type="text" name="name" class="form-control" value=""
                                                            placeholder="Họ v&agrave; t&ecirc;n *" required
                                                            autocomplete="off">
                                                      </div>
                                                      <div class="form-group">
                                                         <input type="text" name="email" class="form-control" value=""
                                                            placeholder="Email *" required autocomplete="off">
                                                      </div>
                                                      <div class="form-group">
                                                         <input type="text" name="phone" class="form-control" value=""
                                                            placeholder="Số điện thoại *" required autocomplete="off">
                                                      </div>
                                                      <div class="form-group">
                                                         <textarea class="form-control" rows="5" name="content"
                                                            placeholder="Nội dung"></textarea>
                                                      </div>
                                                      <div class="text-right">
                                                         <button type="submit"
                                                            class="btn btn-primary">Gửi b&igrave;nh luận</button>
                                                      </div>
                                                   </div>
                                                </form>
                                             </div>
                                             <div class="media  hidden">
                                                <div class="media-left">
                                                   <a href="#">
                                                   <img src=images/ad1.png" alt="" class="media-object">
                                                   </a>
                                                </div>
                                                <div class="media-body">
                                                   <h4 class="media-heading">
                                                      Huyền
                                                      <span>2017-04-01 16:16:46</span>
                                                      <span data-vote-comment="532" class="fbs-vote-comment">
                                                      <a role="button" data-toggle="collapse" href="#comments-532" aria-expanded="false"
                                                         aria-controls="comments-532" class="media-heading-toggle"
                                                         data-name="Huyền">open</a>
                                                      <a href="javascript:voteComment(532, 'up')" title="Th&iacute;ch"
                                                         class="voteup-btn hasTip">
                                                      <i vote-comment-count="1"
                                                         class="vote-comment-txt hasTip-up">(1)</i>
                                                      </a>
                                                      <a href="javascript:voteComment(532, 'down')" title="Kh&ocirc;ng th&iacute;ch"
                                                         class="votedown-btn hasTip">
                                                      <i vote-comment-count="0"
                                                         class="vote-comment-txt hasTip-down">(0)</i>
                                                      </a>
                                                      </span>
                                                   </h4>
                                                   <div class="clearfix">Mình muốn có hướng dẫn fast 2007, bạn có giúp mình đc ko</div>
                                                </div>
                                             </div>
                                             <div class="group">
                                                <div class="media media2 ">
                                                   <div class="media-left">
                                                      <a href="#">
                                                      <img src=images/ad1.png" alt="" class="media-object">
                                                      </a>
                                                   </div>
                                                   <div class="media-body">
                                                      <h4 class="media-heading">
                                                         C&Ocirc;NG TY FAST
                                                         <span>2017-04-03 09:25:02</span>
                                                         <span data-vote-comment="550" class="fbs-vote-comment">
                                                         <a href="javascript:voteComment(550, 'up')" title="Th&iacute;ch"
                                                            class="voteup-btn hasTip">
                                                         <i vote-comment-count="1"
                                                            class="vote-comment-txt hasTip-up">(1)</i>
                                                         </a>
                                                         <a href="javascript:voteComment(550, 'down')" title="Kh&ocirc;ng th&iacute;ch"
                                                            class="votedown-btn hasTip">
                                                         <i vote-comment-count="0"
                                                            class="vote-comment-txt hasTip-down">(0)</i>
                                                         </a>
                                                         </span>
                                                      </h4>
                                                      <div class="clearfix">@Huyền: Chào chị Huyền,<br />
                                                         <br />
                                                         Về tài liệu hướng dẫn Fast Accounting 2007, FAST sẽ liên hệ hỗ trợ chị nhé.<br />
                                                         Trân trọng.
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="collapse" id="comments-532">
                                                <form method="POST" id="form-comment" accept-charset="UTF-8"
                                                   action="/global-actions/create-comment/post/1097">
                                                   <input type="hidden" name="_token" value="os15hPKGJ4spR5wDy4Zfu4k8uZrazwfVznPcJe7M">
                                                   <input type="hidden" name="parent_id" value="532">
                                                   <div class="media media2">
                                                      <div class="form-group">
                                                         <input type="text" name="name" class="form-control" value=""
                                                            placeholder="Họ v&agrave; t&ecirc;n *" required
                                                            autocomplete="off">
                                                      </div>
                                                      <div class="form-group">
                                                         <input type="text" name="email" class="form-control" value=""
                                                            placeholder="Email *" required autocomplete="off">
                                                      </div>
                                                      <div class="form-group">
                                                         <input type="text" name="phone" class="form-control" value=""
                                                            placeholder="Số điện thoại *" required autocomplete="off">
                                                      </div>
                                                      <div class="form-group">
                                                         <textarea class="form-control" rows="5" name="content"
                                                            placeholder="Nội dung"></textarea>
                                                      </div>
                                                      <div class="text-right">
                                                         <button type="submit"
                                                            class="btn btn-primary">Gửi b&igrave;nh luận</button>
                                                      </div>
                                                   </div>
                                                </form>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="comments-more-btn comments-more-btn-parent">
                                          <a href="javascript:;" class="show-all">Xem tất cả</a>
                                          <a href="javascript:;" class="hide-some hidden">Thu gọn</a>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </main>
@endsection
@extends('fontend.layouts.index')
@section('content')
	<main class="main">
               <div class="container">
                  <div class="container">
                     <div class="main-left">
                        <nav class="menu-left aside-left">
                              <h3 class="title-left">MBSOFT Business Online</h3>
                              <ul>
                                 <li>
                                    <a class=""
                                       href="{{ url('/home/tinhnangchung') }}"
                                       title="T&iacute;nh năng chung">Tính năng chung</a>
                                 </li>
                                 
                                 <li>
                                    <a class=""
                                       href="{{ url('/home/dangkydungthu') }}"
                                       title="Đăng k&yacute; d&ugrave;ng thử">Đăng k&yacute; d&ugrave;ng thử</a>
                                 </li>
                                 <li>
                                    <a class=""
                                       href="{{ url('/home/banggiachung') }}"
                                       title="Bảng gi&aacute;">Bảng gi&aacute;</a>
                                 </li>
                                 <li>
                                    <a class="active"
                                       href="{{ url('/home/khachhang') }}"
                                       title="Kh&aacute;ch h&agrave;ng">Kh&aacute;ch h&agrave;ng</a>
                                 </li>
                                 <li>
                                    <a class=""
                                       href="{{ url('/home/download') }}"
                                       title="Download t&agrave;i liệu">Download t&agrave;i liệu</a>
                                 </li>
                                 
                              </ul>
                           </nav>
                        <div class="related-products aside-left">
                              <h3 class="title-left">﻿Sản phẩm c&ugrave;ng nh&oacute;m</h3>
                              <ul>
                                 <li class="media">
                                    <div class="media-left">
                                       <a href="{{ url('/home/banggiasp1') }}"
                                          title="MBSOFT DMS Online"><img src="{{ url('images/DMS.jpg') }}"
                                          alt="MBSOFT DMS Online" width="70"></a>
                                    </div>
                                    <div class="media-body">
                                       <h4 class="media-heading"><a
                                          href="{{ url('/home/banggiasp1') }}"
                                          title="MBSOFT DMS Online">MBSOFT DMS Online</a></h4>
                                    </div>
                                 </li>
                                 
                                 
                                 <li class="media">
                                    <div class="media-left">
                                       <a href="{{ url('home/banggiasp2') }}"
                                          title="MBSOFT Financial"><img src="{{ url('images/sp3.jpg')}}"
                                          alt="MBSOFT Financial" width="70"></a>
                                    </div>
                                    <div class="media-body">
                                       <h4 class="media-heading"><a
                                          href="{{ url('home/banggiasp2') }}"
                                          title="MBSOFT Financial">MBSOFT Financial</a></h4>
                                    </div>
                                 </li>
                                
                              </ul>
                           </div>
                     </div>
                     <div class="main-right">
                        <div class="list-category">
                           <div class="list-category-box">
                              <div class="box-img">
                                 <a href="#"
                                    title="DSKH k&yacute; mua phần mềm ERP MBSOFT Business Online năm 2017" class="img">
                                 <img src="{{ url('images/DSKH-01.jpg')}}" alt="DSKH k&yacute; mua phần mềm ERP MBSOFT Business Online năm 2017">
                                 </a>
                              </div>
                              <div class="box-txt">
                                 <p class="title">
                                    <a href="#"
                                       title="DSKH k&yacute; mua phần mềm ERP MBSOFT Business Online năm 2017">DSKH k&yacute; mua phần mềm ERP MBSOFT Business Online năm 2017</a>
                                 </p>
                                 <div class="infor">13/02/2017 10:52:52 | Số lần xem: 1048</div>
                                 <div class="summary">Danh s&aacute;ch kh&aacute;ch h&agrave;ng k&yacute; mua mới Phần mềm ERP tr&ecirc;n nền web MBSOFT Business Online. FBO l&agrave; giải ph&aacute;p ERP tổng thể bao qu&aacute;t hầu hết c&aacute;c t&aacute;c nghiệp của c&aacute;c ph&ograve;ng ban nghiệp vụ. </div>
                              </div>
                           </div>
                           <div class="list-category-box-simple">
                              <table>
                                 <thead>
                                    <th>Stt</th>
                                    <th>T&ecirc;n b&agrave;i viết</th>
                                    <th>Xem</th>
                                    <th>Ng&agrave;y cập nhật</th>
                                 </thead>
                                 <tbody>
                                    <tr>
                                       <td>1</td>
                                       <td><a href="#" title="DSKH k&yacute; mua phần mềm ERP MBSOFT Business Online năm 2016">DSKH k&yacute; mua phần mềm ERP MBSOFT Business Online năm 2016</a></td>
                                       <td>1732</td>
                                       <td>25/05/2017</td>
                                    </tr>
                                    <tr>
                                       <td>2</td>
                                       <td><a href="#" title="DSKH k&yacute; mua phần mềm ERP MBSOFT Business Online năm 2015">DSKH k&yacute; mua phần mềm ERP MBSOFT Business Online năm 2015</a></td>
                                       <td>1219</td>
                                       <td>06/01/2017</td>
                                    </tr>
                                    <tr>
                                       <td>3</td>
                                       <td><a href="#" title="DSKH phần mềm ERP MBSOFT Business Online k&yacute; mua mới năm 2014">DSKH phần mềm ERP MBSOFT Business Online k&yacute; mua mới năm 2014</a></td>
                                       <td>955</td>
                                       <td>06/01/2017</td>
                                    </tr>
                                    <tr>
                                       <td>4</td>
                                       <td><a href="#" title="DSKH phần mềm ERP MBSOFT Business Online k&yacute; mua mới năm 2013">DSKH phần mềm ERP MBSOFT Business Online k&yacute; mua mới năm 2013</a></td>
                                       <td>885</td>
                                       <td>06/01/2017</td>
                                    </tr>
                                   
                                 </tbody>
                              </table>
                           </div>
                        </div>
                     </div>
                  </div>
            </main>
@endsection
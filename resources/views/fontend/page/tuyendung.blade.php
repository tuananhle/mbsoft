@extends('fontend.layouts.index')
@section('content')
	<main class="main">
                  <div class="container">
                     <div class="container">
                        <div class="main-left">
                           <nav class="menu-left aside-left">
                              <h3 class="title-left">﻿Tuyển dụng</h3>
                              <ul>
                                 <li class="active">
                                    <a title="Tuyển dụng tại MBSOFT H&agrave; Nội"
                                       href="{{ url('home/tuyendung') }}">Tuyển dụng tại MBSOFT H&agrave; Nội</a>
                                 </li>
                              </ul>
                           </nav>
                        </div>
                        <div class="main-right">
                           <div class="list-category">
                              <div class="list-category-box">
                                 <div class="box-img">
                                    <a href="{{ url('home/tuvanungdung/chitiet') }}"
                                       title="MBSOFT tuyển nh&acirc;n vi&ecirc;n tư vấn ứng dụng tại H&agrave; Nội (hết hạn 31-03-2018)" class="img">
                                    <img src="{{url('images/tuyen-dung-fhn-2018.JPG')}}" alt="MBSOFT tuyển nh&acirc;n vi&ecirc;n tư vấn ứng dụng tại H&agrave; Nội (hết hạn 31-03-2018)">
                                    </a>
                                 </div>
                                 <div class="box-txt">
                                    <p class="title">
                                       <a href="{{ url('home/tuvanungdung/chitiet') }}"
                                          title="MBSOFT tuyển nh&acirc;n vi&ecirc;n tư vấn ứng dụng tại H&agrave; Nội (hết hạn 31-03-2018)">MBSOFT tuyển nh&acirc;n vi&ecirc;n tư vấn ứng dụng tại H&agrave; Nội (hết hạn 31-03-2018)</a>
                                    </p>
                                    <div class="summary">CƠ HỘI:
                                       Tư vấn ứng dụng phần mềm, HTTT cho nhiều doanh nghiệp, giải quyết nhiều b&agrave;i to&aacute;n thực tế hay li&ecirc;n quan đến HTTT quản trị doanh nghiệp. N&acirc;ng cao kiến thức, hiểu s&acirc;u về c&aacute;c quy tr&igrave;nh hoạt động kinh doanh của doanh nghiệp.
                                    </div>
                                    <div class="analytics">
                                       <span>Ngày đăng: 2017-06-22 15:27:35</span>
                                       <span>Ngày cập nhật: 2018-04-19 02:01:16</span>
                                       <span>Số lần xem: 2054</span>
                                    </div>
                                 </div>
                              </div>
                              
                              <div class="list-category-box">
                                 <div class="box-img">
                                    <a href="{{ url('home/tuvanungdung/chitiet') }}"
                                       title="MBSOFT tuyển nh&acirc;n vi&ecirc;n tư vấn ứng dụng tại H&agrave; Nội (hết hạn 31-03-2018)" class="img">
                                    <img src="{{url('images/tuyen-dung-fhn-2018.JPG')}}" alt="MBSOFT tuyển nh&acirc;n vi&ecirc;n tư vấn ứng dụng tại H&agrave; Nội (hết hạn 31-03-2018)">
                                    </a>
                                 </div>
                                 <div class="box-txt">
                                    <p class="title">
                                       <a href="{{ url('home/tuvanungdung/chitiet') }}"
                                          title="MBSOFT tuyển nh&acirc;n vi&ecirc;n tư vấn ứng dụng tại H&agrave; Nội (hết hạn 31-03-2018)">MBSOFT tuyển nh&acirc;n vi&ecirc;n tư vấn ứng dụng tại H&agrave; Nội (hết hạn 31-03-2018)</a>
                                    </p>
                                    <div class="summary">CƠ HỘI:
                                       Tư vấn ứng dụng phần mềm, HTTT cho nhiều doanh nghiệp, giải quyết nhiều b&agrave;i to&aacute;n thực tế hay li&ecirc;n quan đến HTTT quản trị doanh nghiệp. N&acirc;ng cao kiến thức, hiểu s&acirc;u về c&aacute;c quy tr&igrave;nh hoạt động kinh doanh của doanh nghiệp.
                                    </div>
                                    <div class="analytics">
                                       <span>Ngày đăng: 2017-06-22 15:27:35</span>
                                       <span>Ngày cập nhật: 2018-04-19 02:01:16</span>
                                       <span>Số lần xem: 2054</span>
                                    </div>
                                 </div>
                              </div>
                              <div class="list-category-box">
                                 <div class="box-img">
                                    <a href="{{ url('home/tuvanungdung/chitiet') }}"
                                       title="MBSOFT tuyển nh&acirc;n vi&ecirc;n tư vấn ứng dụng tại H&agrave; Nội (hết hạn 31-03-2018)" class="img">
                                    <img src="{{url('images/tuyen-dung-fhn-2018.JPG')}}" alt="MBSOFT tuyển nh&acirc;n vi&ecirc;n tư vấn ứng dụng tại H&agrave; Nội (hết hạn 31-03-2018)">
                                    </a>
                                 </div>
                                 <div class="box-txt">
                                    <p class="title">
                                       <a href="{{ url('home/tuvanungdung/chitiet') }}"
                                          title="MBSOFT tuyển nh&acirc;n vi&ecirc;n tư vấn ứng dụng tại H&agrave; Nội (hết hạn 31-03-2018)">MBSOFT tuyển nh&acirc;n vi&ecirc;n tư vấn ứng dụng tại H&agrave; Nội (hết hạn 31-03-2018)</a>
                                    </p>
                                    <div class="summary">CƠ HỘI:
                                       Tư vấn ứng dụng phần mềm, HTTT cho nhiều doanh nghiệp, giải quyết nhiều b&agrave;i to&aacute;n thực tế hay li&ecirc;n quan đến HTTT quản trị doanh nghiệp. N&acirc;ng cao kiến thức, hiểu s&acirc;u về c&aacute;c quy tr&igrave;nh hoạt động kinh doanh của doanh nghiệp.
                                    </div>
                                    <div class="analytics">
                                       <span>Ngày đăng: 2017-06-22 15:27:35</span>
                                       <span>Ngày cập nhật: 2018-04-19 02:01:16</span>
                                       <span>Số lần xem: 2054</span>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </main>
@endsection
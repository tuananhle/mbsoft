@extends('fontend.layouts.index')
@section('content')
	<main class="main">
                  <div class="container">
                     <div class="container">
                        <div class="main-left">
                           <nav class="menu-left aside-left">
                              <h3 class="title-left">Fast Business Online</h3>
                              <ul>
                                 <li>
                                    <a class="active"
                                       href="{{ url('/home/tinhnangchung') }}"
                                       title="T&iacute;nh năng chung">Tính năng chung</a>
                                 </li>
                                 
                                 <li>
                                    <a class=""
                                       href="{{ url('/home/dangkydungthu') }}"
                                       title="Đăng k&yacute; d&ugrave;ng thử">Đăng k&yacute; d&ugrave;ng thử</a>
                                 </li>
                                 <li>
                                    <a class=""
                                       href="{{ url('/home/banggiachung') }}"
                                       title="Bảng gi&aacute;">Bảng gi&aacute;</a>
                                 </li>
                                 <li>
                                    <a class=""
                                       href="{{ url('/home/khachhang') }}"
                                       title="Kh&aacute;ch h&agrave;ng">Kh&aacute;ch h&agrave;ng</a>
                                 </li>
                                 <li>
                                    <a class=""
                                       href="{{ url('/home/download') }}"
                                       title="Download t&agrave;i liệu">Download t&agrave;i liệu</a>
                                 </li>
                                 
                              </ul>
                           </nav>
                           <div class="related-products aside-left">
                              <h3 class="title-left">﻿Sản phẩm c&ugrave;ng nh&oacute;m</h3>
                              <ul>
                                 <li class="media">
                                    <div class="media-left">
                                       <a href="{{ url('/home/banggiasp1') }}"
                                          title="MBSOFT DMS Online"><img src="{{ url('images/DMS.jpg') }}"
                                          alt="MBSOFT DMS Online" width="70"></a>
                                    </div>
                                    <div class="media-body">
                                       <h4 class="media-heading"><a
                                          href="{{ url('/home/banggiasp1') }}"
                                          title="MBSOFT DMS Online">MBSOFT DMS Online</a></h4>
                                    </div>
                                 </li>
                                 
                                 
                                 <li class="media">
                                    <div class="media-left">
                                       <a href="{{ url('home/banggiasp2') }}"
                                          title="MBSOFT Financial"><img src="{{ url('images/sp3.jpg')}}"
                                          alt="MBSOFT Financial" width="70"></a>
                                    </div>
                                    <div class="media-body">
                                       <h4 class="media-heading"><a
                                          href="{{ url('home/banggiasp2') }}"
                                          title="MBSOFT Financial">MBSOFT Financial</a></h4>
                                    </div>
                                 </li>
                                
                              </ul>
                           </div>
                        </div>
                        <div class="main-right">
                           <div class="list-category">
                              <div class="list-category-box">
                                 <div class="box-img">
                                    <a href="#"
                                       title="T&iacute;nh năng lập Ho&aacute; Đơn Điện Tử tr&ecirc;n phần mềm Fast Business Online" class="img">
                                    <img src="{{ url('images/FBO.Sales-Accounting.jpg') }}" alt="T&iacute;nh năng lập Ho&aacute; Đơn Điện Tử tr&ecirc;n phần mềm Fast Business Online">
                                    </a>
                                 </div>
                                 <div class="box-txt">
                                    <p class="title">
                                       <a href="#"
                                          title="T&iacute;nh năng lập Ho&aacute; Đơn Điện Tử tr&ecirc;n phần mềm Fast Business Online">T&iacute;nh năng lập Ho&aacute; Đơn Điện Tử tr&ecirc;n phần mềm Fast Business Online</a>
                                    </p>
                                    <div class="infor">24/04/2017 14:55:42 | Số lần xem: 1344</div>
                                    <div class="summary">T&iacute;nh năng lập Ho&aacute; Đơn Điện Tử (HDDT) cung cấp sự thuận tiện cho doanh nghiệp khi chỉ cần nhập liệu một lần tr&ecirc;n phần mềm Fast Business Online l&agrave; c&oacute; thể tự động gửi dữ liệu ho&aacute; đơn l&ecirc;n phần mềm HDDT của nh&agrave; cung cấp thứ 3.</div>
                                 </div>
                              </div>
                           
                           </div>
                        </div>
                     </div>
                  </div>
               </main>
@endsection
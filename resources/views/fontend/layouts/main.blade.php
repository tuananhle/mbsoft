<main class="main">
                  <div class="container">
                     <nav class="nav-sol">
                        <div class="container">
                           <ul>
                              <li>
                                 <a list-wrap="top-gp-0" class="active" href="javascript:;"
                                    title="Giải ph&aacute;p vừa v&agrave; lớn">
                                 <span>Sản Phẩm Của Chúng Tôi</span>
                                 </a>
                              </li>
                              <li>
                                 <p list-wrap="" class="" href="javascript:;"
                                    title="Giải ph&aacute;p vừa v&agrave; nhỏ">
                                 <span></span>
                                 </p>
                              </li>
                              <li>
                                 <p list-wrap="" class="" href="javascript:;"
                                    title="Giải ph&aacute;p cho đ&agrave;o tạo">
                                 <span></span>
                                 </p>
                              </li>
                           </ul>
                        </div>
                     </nav>
                     <div class="list-wrap active" id="top-gp-0">
                        <div class="container">
                           <div class="swiper-container">
                              <div class="swiper-wrapper">
                                 <div class="swiper-slide">
                                    <figure>
                                       <a href="{{ url('/home/tinhnangchung') }}"
                                          title="MBSOFT Business Online">
                                       <img src="{{url('images/sp2.jpg')}}" class="lazy" alt="MBSOFT Business Online">
                                       </a>
                                    </figure>
                                    <h3>MBSOFT Business Online</h3>
                                    <p>Phần mềm ERP tr&ecirc;n nền web MBSOFT Business Online&nbsp;được ph&aacute;t triển tr&ecirc;n nền tảng web, cho ph&eacute;p truy cập, l&agrave;m việc mọi l&uacute;c, mọi nơi, từ bất kỳ thiết bị n&agrave;o.</p>
                                    <div class="link" style="text-transform: uppercase;">
                                       <a class="btn btn-default"
                                          href="{{ url('/home/tinhnangchung') }}">﻿T&iacute;nh năng</a>
                                       <a class="btn btn-default"
                                          href="{{url('home/dangkydungthu')}}">Đăng k&yacute; d&ugrave;ng thử</a>
                                       <a class="btn btn-default"
                                          href="{{url('home/lienhe')}}">Li&ecirc;n hệ</a>
                                    </div>
                                 </div>
                                 <div class="swiper-slide">
                                    <figure>
                                       <a href="{{ url('/home/tinhnangchung') }}"
                                          title="MBSOFT Business Online">
                                       <img src="{{url('images/sp2.jpg')}}" class="lazy" alt="MBSOFT Business Online">
                                       </a>
                                    </figure>
                                    <h3>MBSOFT Business Online</h3>
                                    <p>Phần mềm ERP tr&ecirc;n nền web MBSOFT Business Online&nbsp;được ph&aacute;t triển tr&ecirc;n nền tảng web, cho ph&eacute;p truy cập, l&agrave;m việc mọi l&uacute;c, mọi nơi, từ bất kỳ thiết bị n&agrave;o.</p>
                                    <div class="link" style="text-transform: uppercase;">
                                       <a class="btn btn-default"
                                          href="{{ url('/home/tinhnangchung') }}">﻿T&iacute;nh năng</a>
                                       <a class="btn btn-default"
                                          href="{{url('home/dangkydungthu')}}">Đăng k&yacute; d&ugrave;ng thử</a>
                                       <a class="btn btn-default"
                                          href="{{url('home/lienhe')}}">Li&ecirc;n hệ</a>
                                    </div>
                                 </div>
                                 <div class="swiper-slide">
                                    <figure>
                                       <a href="{{ url('/home/tinhnangchung') }}"
                                          title="MBSOFT Business Online">
                                       <img src="{{url('images/sp2.jpg')}}" class="lazy" alt="MBSOFT Business Online">
                                       </a>
                                    </figure>
                                    <h3>MBSOFT Business Online</h3>
                                    <p>Phần mềm ERP tr&ecirc;n nền web MBSOFT Business Online&nbsp;được ph&aacute;t triển tr&ecirc;n nền tảng web, cho ph&eacute;p truy cập, l&agrave;m việc mọi l&uacute;c, mọi nơi, từ bất kỳ thiết bị n&agrave;o.</p>
                                    <div class="link" style="text-transform: uppercase;">
                                       <a class="btn btn-default"
                                          href="{{ url('/home/tinhnangchung') }}">﻿T&iacute;nh năng</a>
                                       <a class="btn btn-default"
                                          href="{{url('home/dangkydungthu')}}">Đăng k&yacute; d&ugrave;ng thử</a>
                                       <a class="btn btn-default"
                                          href="{{url('home/lienhe')}}">Li&ecirc;n hệ</a>
                                    </div>
                                 </div>
                                 <div class="swiper-slide">
                                    <figure>
                                       <a href="{{ url('/home/tinhnangchung') }}"
                                          title="MBSOFT Business Online">
                                       <img src="{{url('images/sp2.jpg')}}" class="lazy" alt="MBSOFT Business Online">
                                       </a>
                                    </figure>
                                    <h3>MBSOFT Business Online</h3>
                                    <p>Phần mềm ERP tr&ecirc;n nền web MBSOFT Business Online&nbsp;được ph&aacute;t triển tr&ecirc;n nền tảng web, cho ph&eacute;p truy cập, l&agrave;m việc mọi l&uacute;c, mọi nơi, từ bất kỳ thiết bị n&agrave;o.</p>
                                    <div class="link" style="text-transform: uppercase;">
                                       <a class="btn btn-default"
                                          href="{{ url('/home/tinhnangchung') }}">﻿T&iacute;nh năng</a>
                                       <a class="btn btn-default"
                                          href="{{url('home/dangkydungthu')}}">Đăng k&yacute; d&ugrave;ng thử</a>
                                       <a class="btn btn-default"
                                          href="{{url('home/lienhe')}}">Li&ecirc;n hệ</a>
                                    </div>
                                 </div>
                                 
                                 
                                 
                                 
                                 
                              </div>
                              <div class="swiper-pagination"></div>
                           </div>
                        </div>
                     </div>
                     
                     <div class="content-wrap">
                        <div class="container">
                           <div class="category skin1">
                              <div class="category-nav">
                                 <ul role="tablist">
                                    <li class="active">
                                       <a href="/tin-san-pham"
                                          title="Tin sản phẩm">Tin sản phẩm</a>
                                    </li>
                                    
                                    
                                 </ul>
                              </div>
                              <div class="tab-content">
                                 <div role="tabpanel" class="tab-pane active"
                                    id="news_9">
                                    <div class="row featured">
                                       <div class="col-sm-4">
                                          <div class="cover">
                                             <a href="#" class="img"
                                                title="Phần mềm kế to&aacute;n đ&aacute;m m&acirc;y MBSOFT Accounting Online lần thứ 2 được vinh danh tại Sao Khu&ecirc; 2018">
                                             <img class="img-bg" src="{{url('images/FAO-sao-khue-2018-1200x628.jpg')}}" alt="Phần mềm kế to&aacute;n đ&aacute;m m&acirc;y MBSOFT Accounting Online lần thứ 2 được vinh danh tại Sao Khu&ecirc; 2018">
                                             </a>
                                             <p class="title">
                                                <a href="#"
                                                   title="Phần mềm kế to&aacute;n đ&aacute;m m&acirc;y MBSOFT Accounting Online lần thứ 2 được vinh danh tại Sao Khu&ecirc; 2018">Phần mềm kế to&aacute;n đ&aacute;m m&acirc;y MBSOFT Accounting Online lần thứ 2 được vinh danh tại Sao Khu&ecirc; 2018</a>
                                             </p>
                                             <p class="summary">Sao Khu&ecirc; 2018 c&ograve;n l&agrave; sự kiện đ&aacute;nh dấu sự c&ocirc;ng nhận m&agrave; FAO đạt được sau nhiều năm, th&ocirc;ng qua kết quả kinh doanh v&agrave; số lượng kh&aacute;ch h&agrave;ng ng&agrave;y c&agrave;ng tăng.</p>
                                          </div>
                                       </div>
                                       <div class="col-sm-8">
                                          <div class="row">
                                             <div class="col-sm-6 media-content">
                                                <a href="#" title="Hệ thống FAO đ&atilde; cho ph&eacute;p kết xuất BCTC ra file XML theo Th&ocirc;ng tư 133/2016/TT-BTC" class="media-img">
                                                <img class="img-bg" src="{{url('images/fao-bctc.jpg')}}" alt="Hệ thống FAO đ&atilde; cho ph&eacute;p kết xuất BCTC ra file XML theo Th&ocirc;ng tư 133/2016/TT-BTC" width="77">
                                                </a>
                                                <div class="media-txt">
                                                   <p class="title">
                                                      <a href="#" title="Hệ thống FAO đ&atilde; cho ph&eacute;p kết xuất BCTC ra file XML theo Th&ocirc;ng tư 133/2016/TT-BTC">Hệ thống FAO đ&atilde; cho ph&eacute;p kết xuất BCTC ra file XML theo Th&ocirc;ng tư 133/2016/TT-BTC</a>
                                                   </p>
                                                   <p class="summary">Từ ng&agrave;y 30-03-2018, phi&ecirc;n bản MBSOFT Accounting Online (FAO) đ&atilde; cho ph&eacute;p kết xuất BCTC ra file XML theo Th&ocirc;ng tư 133/2016/TT-BTC để nộp tờ khai qua mạng cho cơ quan Thuế hoặc kết xuất v&agrave;o phần mềm HTKK của Tổng cục Thuế.</p>
                                                </div>
                                             </div>
                                             <div class="col-sm-6 media-content">
                                                <a href="#" class="media-img">
                                                <img class="img-bg" src="{{url('images/phan-mem-quan-tri-doanh-nghiep-1.jpg')}}" alt="Bộ phần mềm quản trị doanh nghiệp to&agrave;n diện" width="77">
                                                </a>
                                                <div class="media-txt">
                                                   <p class="title">
                                                      <a href="/tin-san-pham/bo-phan-mem-quan-tri-doanh-nghiep-toan-dien" title="Bộ phần mềm quản trị doanh nghiệp to&agrave;n diện">Bộ phần mềm quản trị doanh nghiệp to&agrave;n diện</a>
                                                   </p>
                                                   <p class="summary">Bộ c&ocirc;ng cụ phần mềm quản trị doanh nghiệp của C&ocirc;ng ty Phần mềm MBSOFT được ra đời dựa tr&ecirc;n nhu cầu thực tế của c&aacute;c doanh nghiệp về ứng dụng tin học trong t&aacute;c nghiệp v&agrave; quản trị doanh nghiệp.</p>
                                                </div>
                                             </div>
                                             <div class="col-sm-6 media-content">
                                                <a href="#" title="MBSOFT Accounting 11 phi&ecirc;n bản R08 được trang bị th&ecirc;m chức năng &ldquo;Phản hồi&rdquo;" class="media-img">
                                                <img class="img-bg" src="{{url('images/xem-lich-su-phan-hoi-2.png')}}" alt="MBSOFT Accounting 11 phi&ecirc;n bản R08 được trang bị th&ecirc;m chức năng &ldquo;Phản hồi&rdquo;" width="77">
                                                </a>
                                                <div class="media-txt">
                                                   <p class="title">
                                                      <a href="#" title="MBSOFT Accounting 11 phi&ecirc;n bản R08 được trang bị th&ecirc;m chức năng &ldquo;Phản hồi&rdquo;">MBSOFT Accounting 11 phi&ecirc;n bản R08 được trang bị th&ecirc;m chức năng &ldquo;Phản hồi&rdquo;</a>
                                                   </p>
                                                   <p class="summary">Chức năng &ldquo;Phản hồi&rdquo; được cập nhật tr&ecirc;n phần mềm kế to&aacute;n MBSOFT Accounting 11 phi&ecirc;n bản R08.</p>
                                                </div>
                                             </div>
                                            
                                             
                                             <div class="col-sm-6 media-content">
                                                <a href="#" title="C&aacute;c phần mềm của MBSOFT đ&atilde; sẵn s&agrave;ng cho Ho&aacute; đơn điện tử" class="media-img">
                                                <img class="img-bg" src="{{url('images/fas-vnpt-hđt.jpg')}}" alt="C&aacute;c phần mềm của MBSOFT đ&atilde; sẵn s&agrave;ng cho Ho&aacute; đơn điện tử" width="77">
                                                </a>
                                                <div class="media-txt">
                                                   <p class="title">
                                                      <a href="#" title="C&aacute;c phần mềm của MBSOFT đ&atilde; sẵn s&agrave;ng cho Ho&aacute; đơn điện tử">C&aacute;c phần mềm của MBSOFT đ&atilde; sẵn s&agrave;ng cho Ho&aacute; đơn điện tử</a>
                                                   </p>
                                                   <p class="summary">MBSOFT đ&atilde; hợp t&aacute;c c&ugrave;ng VNPT trong việc triển khai t&iacute;nh năng Ho&aacute; Đơn Điện Tử tr&ecirc;n MBSOFT Business Online, MBSOFT Accounting v&agrave; MBSOFT Accounting Online</p>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <br>
                                    <a href="/tin-san-pham">Xem th&ecirc;m</a>
                                 </div>
                              </div>
                           </div>
                           
                           
                           
                        </div>
                     </div>
                     <div class="other-wrap">
                        <div class="container">
                           <div class="row">
                              <div class="col-sm-4 category-client">
                                 <h4 class="title">&Yacute; kiến kh&aacute;ch h&agrave;ng, đối t&aacute;c</h4>
                                 <div class="txt">
                                    <div class="media">
                                       <div class="media-left">
                                          <img class="img-circle" src="{{url('images/tin-thanh.jpg')}}"
                                             alt="Chị Đỗ Thị Ch&acirc;m">
                                       </div>
                                       <div class="media-body">
                                          <p class="media-heading"><a href="#"
                                             title="Chị Đỗ Thị Ch&acirc;m">Chị Đỗ Thị Ch&acirc;m</a>
                                          </p>
                                          <p>Gi&aacute;m đốc T&agrave;i ch&iacute;nh - C&ocirc;ng ty CP Bao b&igrave; T&iacute;n Th&agrave;nh</p>
                                       </div>
                                    </div>
                                    <div class="sumary">Phần mềm mới c&oacute; nhiều t&iacute;nh năng tốt hơn phần mềm cũ. Đ&atilde; r&uacute;t ngắn được hơn ⅔ thời gian khi thực hiện bằng thủ c&ocirc;ng. Phần t&iacute;nh gi&aacute; th&agrave;nh chỉ mất 5 ph&uacute;t so với trước kia phải mất hơn 30 ph&uacute;t. Tuy c&oacute; nhiều ưu điểm nhưng phần mềm vẫn c&oacute; một v&agrave;i điểm cần MBSOFT thực hiện chỉnh sửa để ph&ugrave; hợp với nhu cầu của c&ocirc;ng ty. Điều t&ocirc;i h&agrave;i l&ograve;ng nhất ở MBSOFT l&agrave; con người, c&aacute;c bạn tư vấn triển khai rất tận t&acirc;m v&agrave; nhiệt t&igrave;nh. C&oacute; h&ocirc;m c&aacute;c bạn đ&atilde; thực hiện đến 22h để kịp t&iacute;nh lương cho c&ocirc;ng nh&acirc;n.</div>
                                 </div>
                                 <div class="txt">
                                    <div class="media">
                                       <div class="media-left">
                                          <img class="img-circle" src="{{url('images/ptsc.jpg')}}"
                                             alt="Anh Nguyễn Hữu Hoan">
                                       </div>
                                       <div class="media-body">
                                          <p class="media-heading"><a href=""
                                             title="Anh Nguyễn Hữu Hoan">Anh Nguyễn Hữu Hoan</a>
                                          </p>
                                          <p>Kế to&aacute;n - C&ocirc;ng ty CP Cảng Dịch vụ Dầu kh&iacute; Tổng hợp PTSC Thanh H&oacute;a</p>
                                       </div>
                                    </div>
                                    <div class="sumary">Khi n&oacute;i tới MBSOFT anh h&igrave;nh dung ra 2 vấn đề: thứ nhất l&agrave; sản phẩm MBSOFT v&agrave; hai l&agrave; con người MBSOFT. Tất cả đều t&oacute;m gọn trong những từ ngữ đ&oacute; l&agrave;: Th&ocirc;ng minh - tiện &iacute;ch, dễ thao t&aacute;c &ndash; dễ l&agrave;m quen. Tuy c&oacute; c&aacute;ch trở về địa l&yacute; nhưng anh chưa bao giờ cảm thấy kh&oacute; khăn trong vấn đề sử dụng phần mềm cũng như li&ecirc;n hệ sự hỗ trợ trực tiếp từ MBSOFT.</div>
                                 </div>
                                 <div class="txt">
                                    <div class="media">
                                       <div class="media-left">
                                          <img class="img-circle" src="{{url('images/VNtokai.jpg')}}"
                                             alt="Chị Quỳnh Anh">
                                       </div>
                                       <div class="media-body">
                                          <p class="media-heading"><a href="#"
                                             title="Chị Quỳnh Anh">Chị Quỳnh Anh</a>
                                          </p>
                                          <p>Kế to&aacute;n - Cty TNHH Vina Tokai Powdex</p>
                                       </div>
                                    </div>
                                    <div class="sumary">&quot;C&ocirc;ng ty hiện đang sử dụng phần mềm kế to&aacute;n MBSOFT Accounting So với phần mềm t&ocirc;i đ&atilde; từng sử dụng trước đ&acirc;y th&igrave; phần mềm của MBSOFT tiện &iacute;ch, dễ sử dụng v&agrave; đặc biệt l&agrave; kh&ocirc;ng phải sử dụng c&aacute;c c&acirc;u lệnh. Phần mềm hỗ trợ rất nhiều trong qu&aacute; tr&igrave;nh lập b&aacute;o c&aacute;o t&agrave;i ch&iacute;nh v&agrave; tiết kiệm thời gian cho c&aacute;c c&ocirc;ng việc li&ecirc;n quan đến kế to&aacute;n.
                                       Phần mềm tương đối ph&ugrave; hợp với quy m&ocirc; v&agrave; đặc th&ugrave; của doanh nghiệp.
                                       Dịch vụ của MBSOFT thật chu đ&aacute;o &ndash; mỗi qu&yacute; đều được bộ phận chăm s&oacute;c kh&aacute;ch h&agrave;ng của MBSOFT thăm hỏi về việc sử dụng sản phẩm, bảo tr&igrave; phần mềm. Khi gặp vấn đề được tư vấn tận t&igrave;nh.
                                       Điều mong muốn l&agrave; khi phần mềm c&oacute; thay đổi hoặc được sửa đổi n&acirc;ng cấp th&igrave; nhận được th&ocirc;ng b&aacute;o v&agrave; hướng dẫn c&aacute;ch c&agrave;i phần bổ sung v&agrave;o phần mềm qua email.&quot;
                                    </div>
                                 </div>
                                 <a href="/y-kien-khach-hang-doi-tac">Xem th&ecirc;m</a><br><br>
                              </div>
                              <div class="col-sm-4 category-award">
                                 <h4 class="title">Giải thưởng</h4>
                                 <div class="txt">
                                    <div class="media">
                                       <div class="media-left">
                                          <img class="img-circle" src="{{url('images/aw1.png')}}"
                                             alt="Giải thưởng cho c&aacute;c sản phẩm ">
                                       </div>
                                       <div class="media-body text-justify">
                                          <p class="media-heading">
                                             <a href="#" title="Giải thưởng cho c&aacute;c sản phẩm ">Giải thưởng cho c&aacute;c sản phẩm </a>
                                          </p>
                                          <p>C&aacute;c sản phẩm của MBSOFT đ&atilde; đạt được nhiều giải thưởng uy t&iacute;n như Nh&acirc;n t&agrave;i Đất Việt, Sao Khu&ecirc;, ICT Awards, sản phẩm ti&ecirc;u biểu do người d&ugrave;ng b&igrave;nh chọn...</p>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="txt">
                                    <div class="media">
                                       <div class="media-left">
                                          <img class="img-circle" src="{{url('images/2014.Bangkhen-01.aw.jpg')}}"
                                             alt="Giải thưởng cho c&ocirc;ng ty">
                                       </div>
                                       <div class="media-body text-justify">
                                          <p class="media-heading">
                                             <a href="#" title="Giải thưởng cho c&ocirc;ng ty">Giải thưởng cho c&ocirc;ng ty</a>
                                          </p>
                                          <p>Giải thưởng Top 5 doanh nghiệp CNTT, giải thưởng HCV doanh nghiệp phần mềm c&oacute; doanh số cao, nhiều năm li&ecirc;n tiếp MBSOFT nhận bằng khen của UBND TPHCM v&igrave; c&oacute; th&agrave;nh t&iacute;ch xuất sắc trong XD v&agrave; ph&aacute;t triển ng&agrave;nh CNTT...</p>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="txt">
                                    <div class="media">
                                       <div class="media-left">
                                          <img class="img-circle" src="{{url('images/aw3.png')}}"
                                             alt="Giải thưởng về dịch vụ tư vấn">
                                       </div>
                                       <div class="media-body text-justify">
                                          <p class="media-heading">
                                             <a href="" title="Giải thưởng về dịch vụ tư vấn">Giải thưởng về dịch vụ tư vấn</a>
                                          </p>
                                          <p>Giải thưởng Sao Khu&ecirc; của VINASA về chất lượng tư vấn, dịch vụ phần mềm.</p>
                                       </div>
                                    </div>
                                 </div>
                                 <a href="{{url('home/aw3.png')}}">Xem th&ecirc;m</a><br><br>
                              </div>
                              <div class="col-sm-4 category-comment">
                                 <h4 class="title">Theo d&otilde;i b&igrave;nh luận</h4>
                                 <div class="txt">
                                    <div class="media">
                                       <div class="media-left">
                                          <img class="img-circle" src="{{url('images/ac1.png')}}" alt="">
                                       </div>
                                       <div class="media-body">
                                          <p>
                                             <a href="/download-tai-lieu-gioi-thieu-san-pham/tai-cac-tai-lieu-phan-mem-ke-toan-MBSOFT-accounting"
                                                title="Tải c&aacute;c t&agrave;i liệu phần mềm kế to&aacute;n MBSOFT Accounting">Nguyễn Phương Duy (2018-04-16 14:24:50)</a>
                                          </p>
                                          <p class="media-heading">
                                             <a href="/download-tai-lieu-gioi-thieu-san-pham/tai-cac-tai-lieu-phan-mem-ke-toan-MBSOFT-accounting"
                                                title="Tải c&aacute;c t&agrave;i liệu phần mềm kế to&aacute;n MBSOFT Accounting">Tải c&aacute;c t&agrave;i liệu phần mềm kế to&aacute;n MBSOFT Accounting</a>
                                          </p>
                                          <div class="clearfix">
                                             <p>Mình đang tìm hiểu phần mềm MBSOFT. Rất mong nhận được bản dùng thử của quý doanh nghiệp.<br />
                                                CHÂN THÀNH CẢM ƠN CÁC ANH/CHỊ PHỤ TRÁCH..
                                             </p>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="txt">
                                    <div class="media">
                                       <div class="media-left">
                                          <img class="img-circle" src="{{url('images/ac1.png')}}" alt="">
                                       </div>
                                       <div class="media-body">
                                          <p>
                                             <a href="/bang-gia-phan-mem-ke-toan-MBSOFT-accounting"
                                                title="Bảng gi&aacute; phần mềm kế to&aacute;n MBSOFT Accounting">Đỗ Đức Thắng (2018-04-16 11:35:57)</a>
                                          </p>
                                          <p class="media-heading">
                                             <a href="/bang-gia-phan-mem-ke-toan-MBSOFT-accounting"
                                                title="Bảng gi&aacute; phần mềm kế to&aacute;n MBSOFT Accounting">Bảng gi&aacute; phần mềm kế to&aacute;n MBSOFT Accounting</a>
                                          </p>
                                          <div class="clearfix">
                                             <p>Cần hệ thống kế toán:<br />
                                                Yêu cầu customize theo một số yêu cầu của Công ty<br />
                                                Tích hợp hệ thống Accounting với hệ thống bán hàng của Công ty
                                             </p>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="txt">
                                    <div class="media">
                                       <div class="media-left">
                                          <img class="img-circle" src="{{url('images/ac1.png')}}" alt="">
                                       </div>
                                       <div class="media-body">
                                          <p>
                                             <a href="#"
                                                title="Tải t&agrave;i liệu về phần mềm kế to&aacute;n MBSOFT Accounting 11">Trần Th&uacute;y B&igrave;nh (2018-04-15 08:35:46)</a>
                                          </p>
                                          <p class="media-heading">
                                             <a href="#"
                                                title="Tải t&agrave;i liệu về phần mềm kế to&aacute;n MBSOFT Accounting 11">Tải t&agrave;i liệu về phần mềm kế to&aacute;n MBSOFT Accounting 11</a>
                                          </p>
                                          <div class="clearfix">
                                             <p>MBSOFT có thể gửi cho mình bản dùng thử MBSOFT accounting 11 dc ko. Email mình: thuybinhtranxx@gmail.com. Mình cảm ơn</p>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="txt">
                                    <div class="media">
                                       <div class="media-left">
                                          <img class="img-circle" src="{{url('images/ac1.png')}}" alt="">
                                       </div>
                                       <div class="media-body">
                                          <p>
                                             <a href="#"
                                                title="Phần mềm kế to&aacute;n MBSOFT Accounting cho ng&agrave;nh thương mại">Ngo Dang Khoa (2018-04-13 09:56:17)</a>
                                          </p>
                                          <p class="media-heading">
                                             <a href="#"
                                                title="Phần mềm kế to&aacute;n MBSOFT Accounting cho ng&agrave;nh thương mại">Phần mềm kế to&aacute;n MBSOFT Accounting cho ng&agrave;nh thương mại</a>
                                          </p>
                                          <div class="clearfix">
                                             <p>Quan tam phan mem ke toan online về thương mại, báo giá </p>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </main>
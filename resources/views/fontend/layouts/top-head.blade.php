<div class="top-head">
                     <div class="container">
                        <ul class="pull-left">
                           <li>
                              <i class="fa fa-phone"></i> Hỗ trợ tư vấn sử dụng: <span>1900-2130</span><br>
                              <span class="contact-link"><a href="/lien-he">Liên hệ hỗ trợ ngoài giờ</a></span>
                           </li>
                           <li>
                              <a class="hidden-xs visible-sm visible-md visible-lg" href="mailto:info@mbsoft.com.vn"><i class="fast-envelope"></i> Email: info@mbsoft.com.vn</a>
                              <a class="visible-xs hidden-sm hidden-md hidden-lg" href="mailto:info@mbsoft.com.vn"><i class="fast-envelope"></i> info@fast.com.vn</a>
                           </li>
                        </ul>
                        <ul class="pull-right">
                           <li>
                              <form class="input-group visible-lg-block" action="/tim-kiem" accept-charset="UTF-8" method="GET">
                                 <input type="text" class="form-control" placeholder="Nhập từ kh&oacute;a t&igrave;m kiếm" name="k" value="" style="position: relative; z-index: 1; padding-right: 40px;">
                                 <span class="input-group-btn">
                                 <button class="btn btn-default" type="submit"><i class="fa fa-search"></i></button>
                                 </span>
                              </form>
                              <span class="">
                              <a class=""
                                 onclick="doTranslate('vi|vi');"
                                 href="#" title="Vietnamese">
                              <img src="{{url('images/vi.png')}}"
                                 alt="Vietnamese">
                              </a>
                              </span>
                           </li>
                           <li class="hidden-lg">
                              <!-- Code provided by Google -->
                              <div id="google_translate_element2"></div>
                           </li>
                           <li class="visible-lg-block translation-links">
                              <a href="#" class="gg-changeTrans en" data-lang="en" onclick="doTranslate('vi|en');return false;">
                              </a>
                              <a href="#" class="gg-changeTrans cn" data-lang="zh-CN" onclick="doTranslate('vi|zh-CN');return false;">
                              </a>
                              <a href="#" class="gg-changeTrans fr" data-lang="fr" onclick="doTranslate('vi|fr');return false;">
                              </a>
                              <a href="#" class="gg-changeTrans gm" data-lang="de" onclick="doTranslate('vi|de');return false;">
                              </a>
                              <a href="#" class="gg-changeTrans jp" data-lang="jp" onclick="doTranslate('vi|ja');return false;">
                              </a>
                              <a href="#" class="gg-changeTrans kr" data-lang="kr" onclick="doTranslate('vi|ko');return false;">
                              </a>
                              <a href="#" class="gg-changeTrans ru" data-lang="ru" onclick="doTranslate('vi|ru');return false;">
                              </a>
                           </li>
                        </ul>
                     </div>
                  </div>